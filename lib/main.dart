import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'dart:typed_data';
import 'dart:convert';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';


Map placeHolder = {"hours": "00", "minutes": "00", "name": "", "days": "", "volume": "0"};
void main() => runApp(MyApp());




class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {

  List data = [
  ];





  commands({ @required action, index, budzik }) async{
    final Map command = new Map();

    command["action"] = action;
    if(index != null) command["index"] = index;
    if(budzik != null) command["budzik"] = budzik;


    String address = "B8:27:EB:9D:EA:53";
    List<int> list = json.encode(command).codeUnits;
    Uint8List bytes = Uint8List.fromList(list);


    try {
      BluetoothConnection connection = await BluetoothConnection.toAddress(address);
      connection.output.add(bytes);

      connection.input.listen((Uint8List data) {
        var result = (jsonDecode(ascii.decode(data)));
        print(result);
        if(result != null){
          setState(() {
            this.data = result;
          });
        }

      });

    }
    catch (exception) {
      print('Cannot connect, exception occured');
    }


  }


  @override
  Widget build(BuildContext context) {

    return MaterialApp(
        home: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            title: Text("Sync"),
            backgroundColor: Colors.black,
            actions: [
              IconButton(
                  icon: Icon(Icons.bluetooth, size: 32.0, color: Colors.white),
                  onPressed: () async{
                    await commands(action : "sync");


                  }

              )
            ],

          ),
          body: Center(

              child:  ListView.builder(

                itemCount: data.length,
                itemBuilder: (BuildContext context, int index) {


                  return Slidable(
                    actionPane: SlidableScrollActionPane(),
                    child: InkWell(
                      child: Container(

                        margin: EdgeInsets.all(10.0),
                        color: Colors.grey[850],
                        height: 100,
                        child: Row(

                          children: <Widget>[
                            Container(
                              key: Key(index.toString()),
                              color: Colors.transparent,
                              margin: EdgeInsets.all(15.0),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  '${data[index]["hours"]}:${data[index]["minutes"]}',
                                  style: TextStyle(
                                      fontSize: 48, color: Colors.white),
                                ),
                              ),
                            ),
                            Container(
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  data[index]["name"],
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      onTap: () async{
                        final result = await Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => new Budzik(data[index])),

                        );

                        if(result != ""){
                          setState(() {
                            data[index] = result;
                            commands(action: "change", budzik: result);
                          });


                        }
                      },
                    ),


                    secondaryActions: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 10, bottom: 10),
                        color: Colors.redAccent,
                        child: Align(
                          alignment: Alignment.center,
                          child: IconButton(
                              icon: Icon(Icons.delete_forever, size: 32.0, color: Colors.white),
                              onPressed: () {
                                setState(() {
                                  data.removeAt(index);
                                  commands(action: "remove", index: index);

                                });
                              }
                          ),
                        ),
                      ),
                    ],
                  );
                },
              )




          ),



          floatingActionButton: new Builder (builder: (ctx){
            return FloatingActionButton(
              onPressed: () async{

                final result = await Navigator.push(
                  ctx,
                  MaterialPageRoute(builder: (context) => new Budzik("")),
                );

                if(result != ""){
                  setState(() {
                    print(result);
                    data.add(result);
                    commands(action: "add", budzik: result);
                  });


                }
              },
              child: Icon(Icons.add),
              backgroundColor: Colors.grey,
            );
          }
          ),
        )
    );
  }
}


class Budzik extends StatefulWidget {
  final data;
  Budzik(this.data);

  @override
  State<StatefulWidget> createState() {
    return _BudzikState(data);
  }
}

class _BudzikState extends State<Budzik> with SingleTickerProviderStateMixin{
  var data;
  DateTime myDate;
  _BudzikState(datas){
    if(datas != ""){
      data  = new Map.from(datas);

      myDate = DateTime(
        2000,
        1,
        1,
        int.parse(datas["hours"]),
        int.parse(datas["minutes"]),
      );
    }else{
      myDate = new DateTime.now();
      data = {"hours": myDate.hour.toString(), "minutes": myDate.minute.toString(), "name": "Nazwa budzika", "days": "", "volume": "0"};
      if(myDate.hour < 10){
        data["hours"] = "0${data["hours"]}";
      }

      if(myDate.minute < 10){
        data["minutes"] = "0${data["minutes"]}";
      }
    }

  }






  @override
  Widget build(BuildContext context){
    double width = MediaQuery.of(context).size.width;
    return WillPopScope(
      child: MaterialApp(
          home: Scaffold(
              backgroundColor: Colors.black,
              body: Column(
                  children:[
                    Container(
                        color: Colors.red,
                        child:  Row(
                            children:[
                              Container(
                                width: MediaQuery.of(context).size.width /2,
                                child:FlatButton(
                                  child: Text("Cancel", style: TextStyle(color: Colors.white),),
                                  onPressed: (){

                                    Navigator.pop(context, "");

                                  },
                                ),
                              ),

                              Container(
                                width: MediaQuery.of(context).size.width /2,
                                child:FlatButton(
                                  child: Text("Apply", style: TextStyle(color: Colors.white),),
                                  onPressed: (){

                                    Navigator.pop(context, data);

                                  },
                                ),
                              )
                            ])
                    ),
                    ListView(
                      reverse: true,
                      shrinkWrap: true,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 24.0, right: 24.0, top: 40.0, bottom: 20.0),
                          child: Column(
                            children: <Widget>[
                              FlatButton(
                                  onPressed: () {
                                    DatePicker.showPicker(context, showTitleActions: true,
                                        onConfirm: (date) {
                                          setState(() {
                                            if(date.hour < 10){
                                              data["hours"] = "0${date.hour}";
                                            }else{
                                              data["hours"] = date.hour.toString();
                                            }
                                            if(date.minute < 10){
                                              data["minutes"] = "0${date.minute}";
                                            }else{
                                              data["minutes"] = date.minute.toString();
                                            }
                                          });
                                        },
                                        pickerModel: CustomPicker(myDate, LocaleType.en),
                                        locale: LocaleType.en);
                                  },
                                  child: Text(
                                    '${data["hours"]} : ${data["minutes"]}',
                                    style: TextStyle(color: Colors.white, fontSize: 54,),
                                  )
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 24.0, bottom: 10.0,),  //TODO: luki zrób tutajswoja magie matematyczną zeby ładnie marginy wychodziły miedzy tymi dniami bo ja nie tego. a i łącznie z prawej ma byc 24 marginesu, z lewej tez
                          child: Row(
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.all((2/126)*width),
                                  color: Colors.white10,
                                  width: width/9,
                                  height: 40,
                                  child: Center(
                                    child: Text(
                                      'Pn',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )
                              ),
                              Container(
                                  margin: EdgeInsets.all((2/126)*width),
                                  color: Colors.white10,
                                  width: width/9,
                                  height: 40,
                                  child: Center(
                                    child: Text(
                                      'Wt',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )
                              ),
                              Container(
                                  margin: EdgeInsets.all((2/126)*width),
                                  color: Colors.white10,
                                  width: width/9,
                                  height: 40,
                                  child: Center(
                                    child: Text(
                                      'Śr',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )
                              ),
                              Container(
                                  margin: EdgeInsets.all((2/126)*width),
                                  color: Colors.white10,
                                  width: width/9,
                                  height: 40,
                                  child: Center(
                                    child: Text(
                                      'Czw',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )
                              ),
                              Container(
                                  margin: EdgeInsets.all((2/126)*width),
                                  color: Colors.white10,
                                  width: width/9,
                                  height: 40,
                                  child: Center(
                                    child: Text(
                                      'Pt',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )
                              ),
                              Container(
                                  margin: EdgeInsets.all((2/126)*width),
                                  color: Colors.white10,
                                  width: width/9,
                                  height: 40,
                                  child: Center(
                                    child: Text(
                                      'Sb',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )
                              ),
                              Container(
                                  margin: EdgeInsets.all((2/126)*width),
                                  color: Colors.white10,
                                  width: width/9,
                                  height: 40,
                                  child: Center(
                                    child: Text(
                                      'Nd',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20.0, bottom: 10.0),
                          child:
                          Column(
                            children: <Widget>[
                              Text(
                                'Głośność:',
                                style: TextStyle(color: Colors.white),
                              ),
                              Slider(
                                value: double.parse(data["volume"]),
                                min: 0,
                                max: 100,
                                activeColor: Colors.red,
                                inactiveColor: Colors.white10,
                                onChanged: (double newVolume) {
                                  setState(() {
                                    data["volume"] = newVolume.round();
                                  });
                                },
                              ),
                            ],
                          )
                          ,
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10.0, bottom: 20.0, left: 24.0, right: 24.0),
                          child: TextField(
                            style: new TextStyle(color: Colors.white),
                            decoration: InputDecoration(
                              hintText: '${data["name"]}',
                              hintStyle: TextStyle(color: Colors.white),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.red),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.red),
                              ),
                            ),
                            onSubmitted: (text) {
                              data["name"] = text;
                            },
                          ),
                        ),
                      ].reversed.toList(),
                    )

                  ]
              )

          )
      ),
      onWillPop: () {
        Navigator.pop(context, "");
        return new Future(() => false);
      },);
  }
}

class CustomPicker extends TimePickerModel {
  CustomPicker(DateTime initialTime, LocaleType localType)
      : super(currentTime: initialTime, locale: localType);

  @override
  String rightDivider() => "";

  @override
  List<int> layoutProportions() => [100, 100, 1];
}







//TODO ustawienia budzików : godzina,  dni tygodnia,  opis,  dodatkowe informacje

//TODO wymiana informacji między serverem i aplikacją

//TODO budzik ma budzić


